package com.asad.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.asad.R;
import com.asad.activities.DetailedActivity;
import com.asad.databinding.RowItemBinding;
import com.asad.image.ImageLoader;
import com.asad.models.ItemDto;
import com.creal.solutions.reallib.baseadapters.BaseRecyclerViewAdapter;
import com.creal.solutions.reallib.interfaces.IOnItemClickListner;

import java.util.List;

/**
 * Created by Asad on 9/2/2016.
 */

/**
 * @Code{BaseRecyclerViewAdapter} this is the wrapper of RecyclerView adapter for creating ease in adapter creation.
 * This is my personal code
 */
public class ItemsRecyclerViewAdapter extends BaseRecyclerViewAdapter<ItemDto, RowItemBinding> implements IOnItemClickListner<ItemDto> {
    public ItemsRecyclerViewAdapter(List<ItemDto> data, Context context) {
        super(data, context);
        setOnItemClickListner(this);
    }

    @Override
    protected View onCreateViewHolderDynamic(Context context, LayoutInflater inflater, ViewGroup viewGroup, int viewType) {
        return inflater.inflate(R.layout.row_item, null);
    }

    @Override
    protected void onBindViewHolderDynamic(final ItemDto item, final ViewHolder viewHolder, int position) {
        // Databinding is used for data setting in adapter items
        // this is the custom wrapper class for recycler view
        viewHolder.binding.setItemDto(item);
/**
 * this conditional code block for handling
 */
        if (!item.isCanceled) {
            viewHolder.binding.btnCancel.setText("Cancel");
            ImageLoader.getInstance(context).DisplayImage(item.user.profile_image.large, viewHolder.binding.ivDp, viewHolder.binding.pbLoading, viewHolder.binding.btnCancel);
        } else {
            viewHolder.binding.ivDp.setImageResource(R.drawable.transparentimage);
            viewHolder.binding.btnCancel.setVisibility(View.VISIBLE);
            viewHolder.binding.btnCancel.setText("Cancelled");
            viewHolder.binding.pbLoading.setVisibility(View.GONE);
        }
        viewHolder.binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!item.isCanceled) {
                    viewHolder.binding.ivDp.cancelRunnable();
                    item.isCanceled = true;
                    viewHolder.binding.btnCancel.setText("Cancelled");
                    viewHolder.binding.pbLoading.setVisibility(View.GONE);
                } else {
                    Toast.makeText(context, "Already cancelled", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onViewHolderCreation(RowItemBinding binding) {

    }

    @Override
    protected void onViewHolderCreated(ViewHolder viewHolder) {

    }


    @Override
    public void onRecyclerItemClick(ItemDto model, View view, int position) {
        Intent intent = new Intent(context, DetailedActivity.class);
        intent.putExtra("imageUrl", model.user.profile_image.large);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final ImageView imageView = (ImageView) view.findViewById(R.id.ivDp);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation((Activity) context, imageView, context.getString(R.string.activity_image_trans));
            context.startActivity(intent, options.toBundle());
        } else {
            context.startActivity(intent);
        }
    }
}
