package com.asad.utils;

/**
 * Created by Asad on 9/2/2016.
 */
public enum RequestType {
    JSON(0), XML(1);

    private final int type;

    RequestType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
