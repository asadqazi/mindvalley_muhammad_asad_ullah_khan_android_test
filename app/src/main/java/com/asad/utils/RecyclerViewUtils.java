package com.asad.utils;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by Asad on 9/2/2016.
 */

/**
 * This class helps with recyclerview layout manager, we don't have to write the code again and again
 * just call the required layout manager method
 */
public class RecyclerViewUtils {

    public static LinearLayoutManager getVerticalLinearLayoutManager(Context context) {
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        return manager;
    }

    public static LinearLayoutManager getHorizontalLinearLayoutManager(Context context) {
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return manager;
    }


    public static LinearLayoutManager getGridLayoutManager(Context context, int colCount) {
        GridLayoutManager manager = new GridLayoutManager(context, colCount);
//        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return manager;
    }
}
