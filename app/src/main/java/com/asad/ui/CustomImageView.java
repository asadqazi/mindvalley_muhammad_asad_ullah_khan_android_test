package com.asad.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.util.concurrent.Future;

/**
 * Created by Asad on 9/2/2016.
 */

/**
 * This custom imageview is written just only for keeping the refrence of thread with each imageview while loading the image
 * and if user wants to cancel the image loading he/she can easily cancel the task
 */
public class CustomImageView extends ImageView {
    Future runnable;

    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setRunnable(Future runnable) {
        this.runnable = runnable;

    }

    public void cancelRunnable() {
        if (runnable != null) {
            runnable.cancel(true);
        }
    }
}
