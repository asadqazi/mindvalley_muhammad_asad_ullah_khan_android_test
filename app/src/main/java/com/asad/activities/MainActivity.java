package com.asad.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.asad.R;
import com.asad.adapters.ItemsRecyclerViewAdapter;
import com.asad.api.GetJsonFromServer;
import com.asad.api.GetXMLFromServer;
import com.asad.databinding.ActivityMainBinding;
import com.asad.interfaces.IGetResponseFromServer;
import com.asad.models.ItemDto;
import com.asad.utils.RecyclerViewUtils;
import com.asad.utils.RequestType;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Callback<ArrayList<ItemDto>>, SwipeRefreshLayout.OnRefreshListener {

    ActivityMainBinding binding;
    /**
     * As mentioned in test I have to code so we can support other datatypes in the future as well. I currently coded
     * for two data type/media types that are JSON and XML
     */

    RequestType requestType = RequestType.JSON; // this is by default set to JSON, in future we can add more media type
    // the Enum for specifying either the response is JSON or XML

    // this array is for supporting multiple media type from server. I currently handled for two means JSON and XML
    // and it will automaticcaly call the server for JSON or XML response automatically witout any if or switch conditions.
    // I can add other mediatypes in future as well
    IGetResponseFromServer[] responseFromServers = new IGetResponseFromServer[]{new GetJsonFromServer(this), new GetXMLFromServer(this)};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // I used Android Databinding for views bcz of google new ways
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initGui();
        getItemsFromServer();
       /* if (Build.VERSION.SDK_INT >= 23) {
            checkPermission();
        }*/
    }

    private void getItemsFromServer() {
        binding.innerContent.tvError.setVisibility(View.GONE);
        binding.innerContent.pbLoader.setVisibility(View.VISIBLE);
        responseFromServers[requestType.getType()].getResponse(this);
    }

    private void initGui() {
        setSupportActionBar(binding.toolbar);
        binding.fab.setOnClickListener(this);
        getSupportActionBar().setTitle("MindValley");
        binding.innerContent.swpItems.setColorSchemeColors(Color.BLUE, Color.RED, Color.BLACK);
        binding.innerContent.swpItems.setOnRefreshListener(this);
        binding.innerContent.rcvItems.setLayoutManager(RecyclerViewUtils.getVerticalLinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                break;
        }
    }

    @Override
    public void onResponse(Call<ArrayList<ItemDto>> call, Response<ArrayList<ItemDto>> response) {
        ArrayList<ItemDto> itemsList = response.body();
        cancelSwipe();
        binding.innerContent.pbLoader.setVisibility(View.GONE);
        binding.innerContent.rcvItems.setVisibility(View.VISIBLE);
        binding.innerContent.tvError.setVisibility(View.GONE);

        binding.innerContent.rcvItems.setAdapter(new ItemsRecyclerViewAdapter(itemsList, this));

    }

    @Override
    public void onFailure(Call<ArrayList<ItemDto>> call, Throwable t) {
        if (t.getMessage() != null) {
            Log.e("Error Occured", t.getMessage());
        }
        cancelSwipe();
        binding.innerContent.pbLoader.setVisibility(View.GONE);
        binding.innerContent.rcvItems.setVisibility(View.GONE);
        binding.innerContent.tvError.setVisibility(View.VISIBLE);
    }

    public void cancelSwipe() {
        if (binding.innerContent.swpItems.isRefreshing()) {
            binding.innerContent.swpItems.setRefreshing(false);

        }
    }

    @Override
    public void onRefresh() {
        getItemsFromServer();
    }

    private void checkPermission() {


        if (Build.VERSION.SDK_INT >= 23) {

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(this, "Permission Granted", Toast.LENGTH_LONG).show();
                    // permission was granted, yay! do the
                    // calendar task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
//                    Toast.makeText(this, "You did not allow to access Storage Permission", Toast.LENGTH_LONG).show();
                }
            }
            break;

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }
}
