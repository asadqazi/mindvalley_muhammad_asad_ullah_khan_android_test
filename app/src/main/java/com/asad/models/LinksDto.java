package com.asad.models;

/**
 * Created by Asad on 9/2/2016.
 */

/**
 * This model class is common for User links, categories links and main item model links
 */
public class LinksDto {
    public String self;
    public String html;
    public String photos;
    public String likes;

    public String download;
}
