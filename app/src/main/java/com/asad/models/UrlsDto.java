package com.asad.models;

/**
 * Created by Asad on 9/2/2016.
 */
public class UrlsDto {
    public String raw;
    public String full;
    public String regular;
    public String small;
    public String thumb;
}
