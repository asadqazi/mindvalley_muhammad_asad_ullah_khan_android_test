package com.asad.models;

/**
 * Created by Asad on 9/2/2016.
 */
public class CategoriesDto {
    public int id;
    public String title;
    public String photo_count;
    public LinksDto links;
}
