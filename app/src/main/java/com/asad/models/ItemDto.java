package com.asad.models;

import java.util.ArrayList;

/**
 * Created by Asad on 9/2/2016.
 */
public class ItemDto {

    private static ItemDto instance;
    public String id;
    public String created_at;
    public int width;
    public String color;
    public int likes;
    public boolean liked_by_user;
    public UserDto user;
    public ArrayList<Object> current_user_collections; //The array in response is empty so its difficult to know the array type, so I set to generic Object ancestor of all classes
    public UrlsDto urls;
    public ArrayList<CategoriesDto> categories;
    public LinksDto links;

    // this boolean is for checking if the image loading has been cancelled or not
    public boolean isCanceled = false;

}
