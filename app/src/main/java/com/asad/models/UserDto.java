package com.asad.models;

/**
 * Created by Asad on 9/2/2016.
 */
public class UserDto {
    public String id;
    public String username;
    public String name;
    public UserProfileImageDto profile_image;
    public LinksDto links;
}
