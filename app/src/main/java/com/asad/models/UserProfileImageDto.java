package com.asad.models;

/**
 * Created by Asad on 9/2/2016.
 */
public class UserProfileImageDto {
    public String small;
    public String medium;
    public String large;
}
