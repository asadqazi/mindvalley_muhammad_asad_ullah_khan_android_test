package com.asad.image;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Asad on 9/2/2016.
 */
public class MemoryCache {

    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    // Use 1/8th of the available memory for this memory cache.
    final int cacheSize = maxMemory / 8;
    private LruCache<String, Bitmap> mMemoryCache;

    public MemoryCache() {
        initializeMemCache();
    }

    private void initializeMemCache() {
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public void removeFromCache(String key) {
        mMemoryCache.remove(key);
    }
}
