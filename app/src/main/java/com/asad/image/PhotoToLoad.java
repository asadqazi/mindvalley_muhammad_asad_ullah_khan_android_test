package com.asad.image;

import android.widget.Button;
import android.widget.ProgressBar;

import com.asad.ui.CustomImageView;

  public class PhotoToLoad {
      public final Button btnCancel;
      public String url;
        public CustomImageView imageView;
        public ProgressBar pb;

        public boolean reload = false;

        public PhotoToLoad(String u, CustomImageView i, ProgressBar progressBar, Button btnCancel) {
            url = u;
            imageView = i;
            this.pb = progressBar;
            this.btnCancel = btnCancel;
        }
    }
