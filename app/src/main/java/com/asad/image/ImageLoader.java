package com.asad.image;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.asad.R;
import com.asad.ui.CustomImageView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Asad on 9/2/2016.
 */
public class ImageLoader {

    private static final ScaleType scaleType = ScaleType.FIT_CENTER;
    private static ImageLoader instance;
    private final Context context;

    MemoryCache memCache = new MemoryCache();
    ExecutorService executorService;
    private Map<ImageView, String> imageViews = Collections
            .synchronizedMap(new WeakHashMap<ImageView, String>());
    private int stub_id = R.drawable.transparentimage;

    public ImageLoader(Context context) {
        executorService = Executors.newFixedThreadPool(5);
        this.context = context;
    }

    public static ImageLoader getInstance(Context context) {
        if (instance == null) {
            instance = new ImageLoader(context);
        }
        return instance;
    }

    public void DisplayImage(String url, ImageView imageView,
                             final ProgressBar pb_LoaderIndicator, Button btnCancel) {
        stub_id = R.drawable.transparentimage;
        imageViews.put(imageView, url);
        Bitmap bitmap = memCache.getBitmapFromMemCache(url);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            if (pb_LoaderIndicator != null && btnCancel != null) {
                pb_LoaderIndicator.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
            }
            imageView.setScaleType(scaleType);
        } else {
            if (pb_LoaderIndicator != null && btnCancel != null) {
                pb_LoaderIndicator.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
            }
            queuePhoto(url, (CustomImageView) imageView, pb_LoaderIndicator, btnCancel);
            imageView.setImageResource(stub_id);
            imageView.setScaleType(scaleType);
        }

    }


    private void queuePhoto(String url, CustomImageView imageView, ProgressBar pb, Button btnCancel) {

        PhotoToLoad p = new PhotoToLoad(url, imageView, pb, btnCancel);
        PhotosLoader photoLoader = new PhotosLoader(p);


        Future longRunningTaskFuture = executorService.submit(photoLoader);
        p.imageView.setRunnable(longRunningTaskFuture);
    }

    private Bitmap getBitmap(PhotoToLoad photoToLoad) {
// from memory cache
        if (photoToLoad.reload) {
            return null;
        }
        Bitmap b = memCache.getBitmapFromMemCache(photoToLoad.url);
        if (b != null)
            return b;

        // from web
        try {
            Bitmap bitmap = null;
            URL imageUrl = new URL(photoToLoad.url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();

            BufferedInputStream bufferedInputStream = new BufferedInputStream(is);

            bitmap = BitmapFactory.decodeStream(bufferedInputStream);


            return bitmap;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    public void clearCache() {

    }

    // Task for the queue

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = getBitmap(photoToLoad);

            if (bmp != null) {
                memCache.addBitmapToMemoryCache(photoToLoad.url, bmp);
                if (imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
                Activity a = (Activity) photoToLoad.imageView.getContext();
                a.runOnUiThread(bd);
            }
        }
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null) {
                if (photoToLoad.pb != null && photoToLoad.btnCancel != null) {
                    photoToLoad.pb.setVisibility(View.GONE);
                    photoToLoad.btnCancel.setVisibility(View.GONE);
                }
                photoToLoad.imageView.setImageBitmap(bitmap);
                photoToLoad.imageView.setScaleType(scaleType);
            } else {

                if (stub_id > -1) {
                    photoToLoad.imageView.setImageResource(stub_id);
                }
                photoToLoad.imageView.setScaleType(scaleType);
            }
        }
    }

}
