package com.asad.interfaces;

import com.asad.models.ItemDto;

import java.util.ArrayList;

import retrofit2.Callback;

/**
 * Created by Asad on 9/2/2016.
 */
public interface IGetResponseFromServer {
    public void getResponse(Callback<ArrayList<ItemDto>> ref);
}
