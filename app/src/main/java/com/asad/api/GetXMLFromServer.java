package com.asad.api;

import android.content.Context;

import com.asad.interfaces.IGetResponseFromServer;
import com.asad.models.ItemDto;
import com.asad.utils.RetroFitController;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Asad on 9/2/2016.
 */
public class GetXMLFromServer implements IGetResponseFromServer {
    private final Context context;

    public GetXMLFromServer(Context context) {
        this.context = context;
    }

    @Override
    public void getResponse(Callback<ArrayList<ItemDto>> ref) {

        IGetItems iGetItems = RetroFitController.getRetroInstanceForXml(context).create(IGetItems.class);
        Call<ArrayList<ItemDto>> callApi = iGetItems.getItemsJson();
        callApi.enqueue(ref);

    }
}
