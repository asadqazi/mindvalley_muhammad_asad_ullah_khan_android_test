package com.asad.api;

import com.asad.models.ItemDto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by Asad on 9/2/2016.
 */
//This interface is for retrofit to get response from server
public interface IGetItems {
    @GET("wgkJgazE")
    @Headers("Accept: application/json")
    Call<ArrayList<ItemDto>> getItemsJson();

    @GET("wgkJgazE")
    @Headers("Accept: application/json")
    Call<ArrayList<ItemDto>> getItemsXml();
}
